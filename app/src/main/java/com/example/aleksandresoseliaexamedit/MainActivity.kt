package com.example.aleksandresoseliaexamedit

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.text.isDigitsOnly

class MainActivity : AppCompatActivity() {

    private lateinit var button1: Button
    private lateinit var amount: EditText
    private lateinit var fullName: EditText
    private lateinit var cardNum: EditText
    private lateinit var mm: EditText
    private lateinit var yy: EditText
    private lateinit var ccv: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.hide()

        button1 = findViewById(R.id.button)

        amount = findViewById(R.id.amount)
        fullName = findViewById(R.id.fullName)
        cardNum = findViewById(R.id.cardNum)
        mm = findViewById(R.id.mm)
        yy = findViewById(R.id.yy)
        ccv = findViewById(R.id.ccv)


        button1.setOnClickListener {
            if (!amount.text.isNullOrEmpty() && !fullName.text.isNullOrEmpty() && !cardNum.text.isNullOrEmpty() && !mm.text.isNullOrEmpty() && !yy.text.isNullOrEmpty() && !ccv.text.isNullOrEmpty()){

                if (amount.text.isDigitsOnly() && cardNum.text.isDigitsOnly() && cardNum.text.length == 16 && mm.text.toString().toInt() < 13 && ccv.text.length == 3){

                    val intent = Intent(this, MainActivity2::class.java)

                    intent.putExtra("cardNum",cardNum.text.toString())
                    intent.putExtra("amount",amount.text.toString())
                    intent.putExtra("fullName",fullName.text.toString())
                    intent.putExtra("mm",mm.text.toString())
                    intent.putExtra("yy",yy.text.toString())

                    startActivity(intent)

                    amount.text.clear()
                    fullName.text.clear()
                    cardNum.text.clear()
                    mm.text.clear()
                    yy.text.clear()
                    ccv.text.clear()

                }            else
                    Toast.makeText(this,"შეყვანილი ინფორმაცია არასწორია", Toast.LENGTH_SHORT).show()
            }
            else
                Toast.makeText(this,"შეავსეთ მოცემული ველები", Toast.LENGTH_SHORT).show()

        }
    }


}