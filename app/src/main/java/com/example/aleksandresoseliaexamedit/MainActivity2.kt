package com.example.aleksandresoseliaexamedit

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

private lateinit var button2: Button
private lateinit var amount1: TextView
private lateinit var name: TextView
private lateinit var last4digit: TextView
private lateinit var mm: EditText
private lateinit var yy: EditText
private lateinit var date: TextView


class MainActivity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        supportActionBar?.hide()



        button2 = findViewById(R.id.button2)

        val namefrom = intent.getStringExtra("fullName")
        val amount1from = intent.getStringExtra("amount")
        val last4Digitfrom = intent.getStringExtra("cardNum")
        val mmfrom = intent.getStringExtra("mm")
        val yyfrom = intent.getStringExtra("yy")


        amount1 = findViewById(R.id.amount1)
        name= findViewById(R.id.name)
        last4digit = findViewById(R.id.last4digit)
        date = findViewById(R.id.date)


        amount1.text = amount1from.toString()
        name.text = namefrom.toString()
        if (last4Digitfrom != null) {
            last4digit.text = last4Digitfrom.substring(12).toString()
        }
        date.text = mmfrom.toString() + " " + yyfrom

        button2.setOnClickListener {
            Toast.makeText(this,"თანხა წარმატებით გადაირიცხა", Toast.LENGTH_SHORT).show()



            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}